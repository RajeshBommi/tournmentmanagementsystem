package com.virtusa.tms;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.virtusa.tms.dao.UserDAOImpl;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Point;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;

 class UserTest {
	
	UserDAOImpl u= new UserDAOImpl();
	
	@Test
	void getAllPlayers() {
		List <Player> pList=u.getAllPlayers();
		int before = pList.size();
		Player p=new Player(1,"Rajesh","2000/09/10",23,"RCB","Right Hand","Lest Hand slow");
		pList.add(p);
		int after = pList.size();
		assertEquals(before+1,after );
	}
	@Test
	void getAllPoints() {
		List <Point> list=u.getAllPoints();
		int before = list.size();
		Point p1= new Point("DC",2,1,1,2);
		list.add(p1);
		int after=list.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllSchedule() {
		List<Schedule> sList=u.getAllSchedule();
		int before = sList.size();
		Schedule s=new Schedule(12,"dc vs gt",20,"t20","2000/09/12","dc","gt");
		sList.add(s);
		int after = sList.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllTeams() {
		 List<Team> tList= u.getAllTeams();
		   int before = tList.size();
		   Team t= new Team(120,"hfhf","Buzzer","shaw");
		   tList.add(t);
		   int after=tList.size();
		  assertEquals(before+1,after); 
   }    
	@Test
	void getAllCskSquad() {
		List<CskSquad> s1=u.getAllCskSquad();
		int before = s1.size();
		CskSquad cs=new CskSquad(14,"Ravi","2000/03/12",23,"Right","LeftHand"); 
		s1.add(cs);
		int after = s1.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllRcbSquad() {
		List<RcbSquad> s1=u.getAllRcbSquad();
		int before = s1.size();
		RcbSquad cs=new RcbSquad(14,"Ravi","2000/03/12",23,"Right","LeftHand"); 
		s1.add(cs);
		int after = s1.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllDcSquad() {
		List<DcSquad> s1=u.getAllDcSquad();
		int before = s1.size();
		DcSquad cs=new DcSquad(14,"Ravi","2000/03/12",23,"Right","LeftHand"); 
		s1.add(cs);
		int after = s1.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllGtSquad() {
		List<GtSquad> s1=u.getAllGtSquad();
		int before = s1.size();
		GtSquad cs=new GtSquad(14,"Ravi","2000/03/12",23,"Right","LeftHand"); 
		s1.add(cs);
		int after = s1.size();
		assertEquals(before+1, after);
	}
	@Test
	void getAllSrhSquad() {
		List<SrhSquad> s1=u.getAllSrhSquad();
		int before = s1.size();
		SrhSquad cs=new SrhSquad(14,"Ravi","2000/03/12",23,"Right","LeftHand"); 
		s1.add(cs);
		int after = s1.size();
		assertEquals(before+1, after);
	}
	@Test
	   void searchTeamByName() {
		   Team t=u.searchTeamByName("CSK");
		   assertEquals("CSK",t.getTeamName());
	   }
	@Test
	void searchScheduleByDate() {
		 List <Schedule> s=u.searchScheduleByDate("2023/03/31");
		 int before=s.size();
		 Schedule s1= new Schedule(12,"dc vs gt",20,"t20","2000/09/12","dc","gt");
		 s.add(s1);
		 int after =s.size();
		 assertEquals(before+1, after);
	}

}
