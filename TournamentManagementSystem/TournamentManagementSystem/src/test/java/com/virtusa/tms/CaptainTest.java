package com.virtusa.tms;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;

import com.virtusa.tms.dao.CaptainDAOImpl;
import com.virtusa.tms.dto.Player;

import com.virtusa.tms.dto.Team;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

 class CaptainTest {
	CaptainDAOImpl c = new CaptainDAOImpl();
	
	@Test
	void addPlayer() {
		Player p= new Player(1,"Rajesh","2000/09/10",23,"RCB","Right Hand","Lest Hand slow");
		assertEquals(true,c.addPlayer(p));
	}
	
	@Test
	void SearchPlayer() {
		Player p=c.searchPlayer(1);
		assertEquals("Rajesh",p.getName());
	}
	@Test
	void ModifyPlayer() {
		Player p=c.searchPlayer(1);
		p.setName("Nithish");
		assertEquals("Rajesh",c.searchPlayer(1).getName());
	}
	@Test
	void RemovePlayer() {
		assertEquals(true, c.removePlayer(1));
	}
	@Test
	void getAllPlayers() {
		List <Player> pList=c.getAllPlayers();
		int before = pList.size();
		Player p=new Player(1,"Rajesh","2000/09/10",23,"RCB","Right Hand","Lest Hand slow");
		pList.add(p);
		int after = pList.size();
		assertEquals(before+1,after );
	}
	@Test
	   void getAllTeams() {
			   List<Team> tList= c.getAllTeams();
			   int before = tList.size();
			   Team t= new Team(121,"hf","Buer","saw");
			   tList.add(t);
			   int after=tList.size();
			  assertEquals(before+1,after); 
	  }

}
