package com.virtusa.tms;

import static org.junit.Assert.assertEquals;

import java.util.List;




import org.junit.runners.MethodSorters;

import com.virtusa.tms.dao.AdminDAOImpl;
import com.virtusa.tms.dao.UserDAOImpl;
import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.dto.User;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
  class AdminTest {
	
	UserDAOImpl u= new UserDAOImpl();
	AdminDAOImpl a=new AdminDAOImpl();
       
	   @Test
	    void addTeam() {
		  Team t= new Team(108,"DL","Buzzer","shaw");
		   assertEquals(true,a.addTeam(t));
	   }
	   
	   @Test
	   void removeTeam() {
		   assertEquals(true, a.removeTeam(108,"DL"));
	   }
	   
	   @Test
	   void searchTeamByName() {
		   Team t=u.searchTeamByName("CSK");
		   assertEquals("CSK",t.getTeamName());
	   }
	   
	   @Test
	   void searchTeambyId() {
		   Team t=a.searchTeam(105);
		   assertEquals("GT", t.getTeamName());
	   }
	  
	   @Test
	   void modifyTeam() {
		   Team t=a.searchTeam(101);
		   t.setViceCaptain("Mayers");
		   assertEquals("Deepak", a.searchTeam(101).getViceCaptain());
	   }
	   
	   @Test
	   void getAllTeams() {
			   List<Team> tList= a.getAllTeams();
			   int before = tList.size();
			   Team t= new Team(120,"hfhf","Buzzer","shaw");
			   tList.add(t);
			   int after=tList.size();
			  assertEquals(before+1,after); 
	  }
	   
	   @Test
	   void scheduleMatch() {
		   Schedule s= new Schedule();
		   s.setMatchid(6);
		   s.setTeams("Rcb vs RR");
		   s.setOvers(20);
		   s.setMatchType("t20");
		   s.setTime("2023/04/17 03:30:24");
		   s.setWinner("Rcb");
		   s.setLoser("RR");
		   assertEquals(true, a.scheduleMatch(s));
	   }
	   
	   @Test
	   void testCancelMatch() {
		   assertEquals(true,a.cancelMatch(6));
	   }

	   @Test	   
	   void addFee() {
		   EntryFee  ef= new EntryFee(2,10000);
		   assertEquals(true,a.addFee(ef));
	   }
	   
	   @Test
	   void modifyFee() {
		   EntryFee ef=new EntryFee(2,5000);
		   assertEquals(true,a.modifyFee(ef));
	   }
	   @Test
	   void removeFee() {
		   assertEquals(true, a.removeFee(2));
	   }
	   @Test
	   void getFee() {
		   List<EntryFee> tList= a.getFee();
		   int before = tList.size();
		   EntryFee t= new EntryFee(2,10000);
		   tList.add(t);
		   int after=tList.size();
		  assertEquals(before+1,after); 
	   }
	   @Test
	   void getMatchById() {
		   Schedule s= a.getMatchById(1);
		   assertEquals("csk vs dc",s.getTeams());
	   }
	   @Test
	   void adminLogin() {
		   int u=a.adminLogin("Rajesh123", "123456");
		   assertEquals(u, a.adminLogin("Rajesh123", "123456"));
	   }
	   @Test
	   void SearchUser() {
		   User u=a.searchUserbyUsername("Ravi123");
		   assertEquals("Ravi123", u.getUserName());
	   }
	   
}
