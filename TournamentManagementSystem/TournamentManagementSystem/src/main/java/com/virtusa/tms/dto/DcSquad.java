package com.virtusa.tms.dto;

public class DcSquad {
	private int playeri;
	private String playername;
	private String playerDOB;
	private int playerAge;
	private String playerBatStyle;
	private String playerBowlStyle; 
	
	public DcSquad() {
		super();
	}

	public DcSquad(int playerid, String name, String dOB, int age,  String batStyle, String bowlStyle) {
		super();
		this.playeri = playerid;
		this.playername = name;
		this.playerDOB = dOB;
		this.playerAge = age;
		this.playerBatStyle = batStyle;
		this.playerBowlStyle = bowlStyle;
	}

	public int getPlayerid() {
		return playeri;
	}

	public void setPlayerid(int playerid) {
		this.playeri = playerid;
	}

	public String getName() {
		return playername;
	}

	public void setName(String name) {
		this.playername = name;
	}

	public String getdOB() {
		return playerDOB;
	}

	public void setdOB(String dOB) {
		this.playerDOB = dOB;
	}

	public int getAge() {
		return playerAge;
	}

	public void setAge(int age) {
		this.playerAge = age;
	}

	public String getBatStyle() {
		return playerBatStyle;
	}

	public void setBatStyle(String batStyle) {
		this.playerBatStyle = batStyle;
	}

	public String getBowlStyle() {
		return playerBowlStyle;
	}

	public void setBowlStyle(String bowlStyle) {
		this.playerBowlStyle = bowlStyle;
	}

	@Override
	public String toString() {
		return "Player [playerid=" + playeri + ", name=" + playername + ", dOB=" + playerDOB + ", age=" + playerAge + ", "
				+ " batStyle=" + playerBatStyle + ", bowlStyle=" + playerBowlStyle + "]";
	}

}
