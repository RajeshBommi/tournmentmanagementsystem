package com.virtusa.tms.service;


import java.util.List;


import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.dto.User;

public   interface IAdmin {
	public  boolean addTeam(Team t);
	public Team searchTeam(int teamid);
	public boolean removeTeam(int teamid,String tname);
	public boolean modifyTeam(Team t);
	public boolean cancelMatch(int matchid);
	public boolean scheduleMatch(Schedule m);
	public List<Team>getAllTeams();
	public boolean addFee(EntryFee feeAmount);
	public boolean modifyFee(EntryFee e);
	public boolean removeFee(int feeid);
	public List<EntryFee> getFee();
	public Team searchTeam(String teamName);
	public Schedule getMatchById(int matchid);
	public int adminLogin(String userName,String userPassword);
	public User searchUserbyUsername(String userName);
	
}
