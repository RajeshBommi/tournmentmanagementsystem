package com.virtusa.tms.controller;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.util.AppContext;


public class MainMenu {
	
	private static Logger log=LogManager.getLogger(MainMenu.class);


	public void mainMenu() {
		int ch=0;
		Scanner sc =new Scanner(System.in);
		do {
			log.info("***************Welcome to Cricket World***************");
			log.info("1.Admin");
			log.info("2.Captain");
			log.info("3.User");
			log.info("4.Exit");
			log.info("Enter your Option");
			ch=sc.nextInt();
			
			LoginController lc =AppContext.getInstance().getObject("LoginController");
			
			switch(ch) { 
			case 1:lc.adminLogin();
			        break;
			case 2:lc.captainLogin();
			        break;
		case 3:lc.userLogin();
			       break;
			case 4: log.info("Successfully Exited!");
					return;
			default:log.info("Invalid option!");
			}
			
		}
		while(ch<4);
		sc.close();
			
		}
	}


