package com.virtusa.tms.service;


import java.util.List;

import com.virtusa.tms.dao.AdminDAOImpl;
import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.dto.User;
import com.virtusa.tms.util.AppContext;

public class AdminServiceImpl implements IAdmin {
	
	AdminDAOImpl  a= AppContext.getInstance().getObject("IAdminDAOImpl");
	
	public boolean addTeam(Team t) {
		return a.addTeam(t);
		
	}
	public Team searchTeam(int n) {
		return a.searchTeam(n);
	}
	
	public boolean removeTeam(int teamid,String tname) {
		return a.removeTeam(teamid,tname);
		}

	public boolean modifyTeam(Team t) {
		return a.modifyTeam(t);
	}
	public boolean cancelMatch(int t) {
		return a.cancelMatch(t);
	}
	public  List<Team> getAllTeams(){
	    return a.getAllTeams();
		
	}
	public boolean addFee(EntryFee f) {
		return a.addFee(f);
	}
	public boolean modifyFee(EntryFee f) {
		return a.modifyFee(f);
	}
	public boolean removeFee(int feeid) {
		return a.removeFee(feeid);
	}
	public boolean scheduleMatch(Schedule m) {
		return a.scheduleMatch(m);
	}
	public List<EntryFee> getFee() {
		return a.getFee();
	}
	
	public Team searchTeam(String teamName) {
		return a.searchTeam(teamName);
	}
	
	public Schedule getMatchById(int matchid) {
	return a.getMatchById(matchid);
	}
	
	public int adminLogin(String userName, String userPassword) {
		return a.adminLogin(userName, userPassword);
	}
	
	public User searchUserbyUsername(String userName) {
		return a.searchUserbyUsername(userName);
	}
	



}
