package com.virtusa.tms.exception;

public class PlayerNotFoundException extends Exception{
	
	public PlayerNotFoundException(String s) {
		
		super(s);
	}

}
