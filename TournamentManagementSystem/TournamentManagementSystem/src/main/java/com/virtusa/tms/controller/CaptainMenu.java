package com.virtusa.tms.controller;


import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.exception.PlayerNotFoundException;
import com.virtusa.tms.exception.TeamAlreadyExistsException;
import com.virtusa.tms.exception.TeamNotFoundException;

import com.virtusa.tms.service.CaptainServiceImpl;
import com.virtusa.tms.util.AppContext;

public  class CaptainMenu {

	CaptainServiceImpl c= AppContext.getInstance().getObject("CaptainService");
   
	
	
	
	private static Logger log=LogManager.getLogger(CaptainMenu.class);
	Scanner sc=new Scanner(System.in);
	
	 public int inpout()
	  {
		  String choice = sc.next();
		  int ch ;
		  if(choice.matches("[0-9]*"))
			  {
			   	ch = Integer.parseInt(choice);
			  	return ch;
			  }
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return inpout();
		  }
	  }
	 public String stringinpout()
	  {
		  String choice = sc.next();
		  
		  if(choice.matches("[a-zA-Z]*"))
			  	return choice;
			  
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return stringinpout();
		  }
	  }
	 public void cmenu() {
		 log.info("***************Captain Management Menu*******************");
			log.info("1.Add Player");
			log.info("2.Search Player");
			log.info("3.Modify Player Details");
			log.info("4.Remove Player");
			log.info("5.List Of Players");
			log.info("6.List of Teams");
			log.info("7.BAck To Main Menu");
			log.info("Enter your Option");
	 }
	 public void addPlayer() {
		 log.info("Enter PlayerId");
	    int  playerid=sc.nextInt();
	     if(c.searchPlayer(playerid)==null) {
	     log.info("Enter Player Name to be added");
	    String name=sc.next();
	     log.info("Enter the Date Of Birth");
	   String  dOB=sc.next();
	     log.info("Enter the age of the player");
	   int  age=sc.nextInt();
	     log.info("Enter the TeamName");
	    String team=sc.next();
	     log.info("Enter The BatStyle");
	    String batStyle=sc.next();
	     log.info("Enter the BowlStyle");
	   String  bowlStyle=sc.next();
	     Player p=new Player(playerid,name,dOB,age,team,batStyle,bowlStyle);
	     if(c.addPlayer(p)) {
	    	 log.info("Player added Successfully");
	     }
	     else {
	    	 log.info("Sorry Player doesn't exist");
	     }
	     }
	 	else {
			try {
				throw new TeamAlreadyExistsException("Player alreadyexists");
				}
			catch(Exception r) {
				log.error(r);
			}
		}
	 }
	 public void searchPlayer() {
		 log.info("Enter the Playerid to be searched: ");
	      int playerid=sc.nextInt();
	       Player pl=c.searchPlayer(playerid);
	       if(pl==null){
	    	   try {
					throw new PlayerNotFoundException("Player does not exist");
				}
				catch(Exception e) {
					log.error(e);
					}
			}
			else {
				log.info(pl);
			}
	 }
	 public void modifyPlayer() {
		 log.info("Enter the playerid to be modified");
	       int pid=sc.nextInt();
	       Player temp1=c.searchPlayer(pid);
			if(temp1!=null) {
	       log.info("Enter the Player Name");
	    String  pName=sc.next();
	       log.info("Enter the Date Of Birth");
		   String  pDOB=sc.next();
		     log.info("Enter the age of the player");
		    int pAge=sc.nextInt();
		     log.info("Enter the new TeamName");
		    String teamName=sc.next();
		     log.info("Enter The BatStyle");
		   String  pBatStyle=sc.next();
		     log.info("Enter the BowlStyle");
		   String  pBowlStyle=sc.next();
		     Player pa=new Player(pid,pName,pDOB,pAge,teamName,pBatStyle,pBowlStyle);
		     if(c.modifyPlayer(pa)) {
		    	 log.info("Player Modified Successfully");
		     }
		     else {
		    	 log.info("Sorry Player modification is failed");
		     }
			}
			else {
				try {
				throw new PlayerNotFoundException("Sorry Playerid does not exist");
			}
				catch(Exception e) {
					log.error(e);
				}
				}
	 }
	 public void removePlayer() {
		 log.info("Enter the Playerid to remove");
	     int  playerid=sc.nextInt();
	       if(c.removePlayer(playerid)){
	    	   log.info("Player removed from the successfully");
	       }
	       else {
	    	   try {
					throw new PlayerNotFoundException("Sorry Playerid does not exist in table");
				}
				catch(Exception t) {
					log.error(t);
					
				}
	       }
	 }
	 public void getAllPlayers() {
		 List<Player> l=c.getAllPlayers();
	      if(!l.isEmpty()) {
	    	  l.stream().forEach(i->log.info(i));
	      }
	      
	      else {
	        	try {
	        		throw new PlayerNotFoundException("Players doesn't exist");
	        	}
	        	catch(Exception o) {
	        		log.error(o);
	        	}
	        }
	 }
	 public void getAllTeams() {
		 List<Team> tl=c.getAllTeams();
	      if(!tl.isEmpty()) {
	    	  tl.stream().forEach(i->log.info(i));
	      }
	      else {
	        	try {
	        		throw new TeamNotFoundException("Teams doesn't exist");
	        	}
	        	catch(Exception o) {
	        		log.error(o);
	        	}
	        }
	 }

 }

