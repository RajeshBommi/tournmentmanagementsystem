package com.virtusa.tms.dao;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.User;

import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.util.DbConnection;

public class AdminDAOImpl extends DbConnection{
	private static Logger log=LogManager.getLogger(AdminDAOImpl.class);
	
	
	public boolean addTeam(Team t) {
		
		String cmd="insert into team values(?,?,?,?)";
		String cmd1="insert into points values(?,?,?,?,?)";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);
				PreparedStatement ps1=getConnection().prepareStatement(cmd1);)
		{
			ps.setInt(1, t.getTeamid());
			ps.setString(2, t.getTeamName());
			ps.setString(3,t.getCaptain() );
			ps.setString(4, t.getViceCaptain());
			ps1.setString(1, t.getTeamName());
			ps1.setInt(2, 0);
			ps1.setInt(3, 0);
			ps1.setInt(4, 0);
			ps1.setInt(5, 0);
			
			ps.executeUpdate();
			ps1.executeUpdate();
			return true;
		}
		catch(Exception e) {
			log.info(e.getMessage());
			
		}
		return false;
	}
	
	public Team searchTeam(int teamid) {
		String cmd="select * from team where teamId=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);) {
			ps.setInt(1, teamid);
			ResultSet rs=ps.executeQuery();
			if(rs.next()) {
				int id=rs.getInt(1);
				String teamName=rs.getString(2);
				String captain =rs.getString(3);
				String viceCaptain=rs.getString(4);
				return new Team(id,teamName,captain,viceCaptain);
			}
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
	
	public boolean removeTeam(int teamid,String tname) {
		String cmd="delete from team where Teamid=?";
		String cmd1="delete from points where Team=?";
		try (PreparedStatement ps = getConnection().prepareStatement(cmd);PreparedStatement ps1 = getConnection().prepareStatement(cmd1);){
			ps.setInt(1, teamid);
			ps1.setString(1,tname);
			ps.executeUpdate();
			ps1.executeUpdate();
			return true;
		}
			catch(Exception e) {
				log.info(e.getMessage());
				
			}
		return false;
		
	}
	public boolean modifyTeam(Team t) {
		String cmd="update team set teamname=?,captain=?,viceCaptain=? where teamId=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);) {
			ps.setInt(4, t.getTeamid());
			ps.setString(1,t.getTeamName());
			ps.setString(2,t.getCaptain());
			ps.setString(3,t.getViceCaptain());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	
	public boolean cancelMatch(int matchid) {
		String cmd="delete from schedule where Matchid=?";
		try(PreparedStatement ps =getConnection().prepareStatement(cmd);) {
            ps.setInt(1, matchid);
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean scheduleMatch(Schedule m) {
		String cmd="insert into schedule values(?,?,?,?,?,?,?)";
		
		try(PreparedStatement ps =getConnection().prepareStatement(cmd);) {
			ps.setInt(1, m.getMatchid());
			ps.setString(2, m.getTeams());
			ps.setInt(3,m.getOvers());
			ps.setString(4,m.getMatchType());
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date parsed = format.parse(m.getTime());
			java.sql.Timestamp sql = new java.sql.Timestamp(parsed.getTime());
			ps.setTimestamp(5, sql);
			ps.setString(6,m.getWinner());
			ps.setString(7,m.getLoser());
			ps.executeUpdate();
			String[] al=m.getTeams().split("vs");
			
			for(String i:al) {
				
				String cmd1="update points set played=played+1 where team=?";
				try(PreparedStatement ps1=getConnection().prepareStatement(cmd1);){
				ps1.setString(1,i.trim());
				ps1.executeUpdate();
			}
			}
			String cmd2="update points set Won=Won+1,Pts=Pts+2  where team=?";
			try(PreparedStatement ps2=getConnection().prepareStatement(cmd2);){
			ps2.setString(1,m.getWinner());
			ps2.executeUpdate();
			}
			String cmd3="update points set lose=lose+1  where team=?";
			try(PreparedStatement ps3=getConnection().prepareStatement(cmd3);){
			ps3.setString(1,m.getLoser());
			ps3.executeUpdate();
			return true;
		}
		}
			
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public  List<Team> getAllTeams(){
		String cmd="select * from team";
		List<Team> teamList=new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);) {
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int teamid=(rs.getInt(1));
				String teamName=(rs.getString(2));
				String captain=(rs.getString(3));
				String viceCaptain =(rs.getString(4));
				Team t= new Team(teamid,teamName,captain,viceCaptain);
				teamList.add(t);
			}
			return teamList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return teamList;
		
	}
	public boolean addFee(EntryFee f) {
		String cmd="insert into entryfee values(?,?)";
		try (PreparedStatement ps=getConnection().prepareStatement(cmd);){
			ps.setInt(1,f.getFeeid());
			ps.setInt(2, f.getFeeAmount());
			ps.executeUpdate();
			return true;
		}
		catch(Exception e){
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifyFee(EntryFee f) {
		String cmd="Update entryfee set FeeAmount=? where Feeid=?";
		try(PreparedStatement ps =getConnection().prepareStatement(cmd);) {
			ps.setInt(1, f.getFeeid());
			ps.setInt(2, f.getFeeAmount());
			ps.executeUpdate();
			return true;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean removeFee(int feeid) {
		String cmd="delete from entryfee where feeid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);) {
			ps.setInt(1, feeid);
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public List<EntryFee> getFee() {
			String cmd="select * from entryfee ";
			List<EntryFee> ef=new ArrayList<>();
			try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
			{
				
				ResultSet rs=ps.executeQuery();
				while(rs.next()) {
					int fid=rs.getInt(1);
					int feea=rs.getInt(2);
					EntryFee e=new EntryFee(fid,feea);
					 ef.add(e);
				}
				return ef;
				
			}
			catch(Exception e) {
				log.info(e.getMessage());
			}
			return ef;
		}
	public Team searchTeam(String teamName) {
		String cmd="select * from team where teamname=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setString(1, teamName);
            ResultSet rs =ps.executeQuery();
            if(rs.next()) {
            	int id=rs.getInt(1);
            	String team=rs.getString(2);
            	String captain=rs.getString(3);
            	String vice=rs.getString(4);
            	return new Team(id,team,captain,vice);
            }
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
	public Schedule getMatchById(int matchid) {
		String cmd="select * from schedule where Matchid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(1, matchid);
            ResultSet rs =ps.executeQuery();
            if(rs.next()) {
            	int id=rs.getInt(1);
            	String team=rs.getString(2);
            	int overs=rs.getInt(3);
            	String matchType=rs.getString(4);
            	String time=rs.getString(5);
            	String winner=rs.getString(6);
            	String loser=rs.getString(7);
            	return new Schedule(id,team,overs,matchType,time,winner,loser);
            }
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
	public int adminLogin(String userName,String userPassword) {
	 String cmd="select * from admin where Username=? and UserPassword=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setString(1,userName);
			ps.setString(2, userPassword);
			ResultSet rs=ps.executeQuery();
		     if(rs.next()) {
		    	 return 1;
			} 	
   }
		catch(Exception e){
			log.info(e.getMessage());
		}
		return 0;
	}
	public User searchUserbyUsername(String userName) {
		String cmd="select * from admin where Username=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setString(1, userName);
            ResultSet rs =ps.executeQuery();
            if(rs.next()) {
              String  name=rs.getString(1);
               int id=rs.getInt(2);
               String uName=rs.getString(3);
               String uPass=rs.getString(4);
               String place=rs.getString(5);
               String type=rs.getString(6);
               return new User(name,id,uName,uPass,place,type);
                 }
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
   
}
