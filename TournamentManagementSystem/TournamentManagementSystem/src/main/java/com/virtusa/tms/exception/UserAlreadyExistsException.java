package com.virtusa.tms.exception;

public class UserAlreadyExistsException extends Exception {

	public  UserAlreadyExistsException (String s) {
		
		super(s);
	}

}
