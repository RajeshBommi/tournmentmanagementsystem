package com.virtusa.tms.util;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




public  class DbConnection {
	
	private static final String ROOT = "root";
	
	protected DbConnection() {
		
	}
	
	private static Logger log=LogManager.getLogger(DbConnection.class);
	
	public static final String USER=ROOT; 	
	public static final String PASS=ROOT; 
	
	public static Connection getConnection(){
		try {
		
		      Class.forName("com.mysql.cj.jdbc.Driver");
			
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/TMS",USER,PASS);
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}

}

