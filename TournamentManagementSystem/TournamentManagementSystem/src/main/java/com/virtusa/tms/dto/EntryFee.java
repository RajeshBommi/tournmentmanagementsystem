package com.virtusa.tms.dto;

public class EntryFee {
private	int feeid;
	private int feeAmount;
	
	
	public EntryFee() {
		super();
	}


	public EntryFee(int feeid, int feeAmount) {
		super();
		this.feeid = feeid;
		this.feeAmount = feeAmount;
	}


	public int getFeeid() {
		return feeid;
	}


	public void setFeeid(int feeid) {
		this.feeid = feeid;
	}


	public int getFeeAmount() {
		return feeAmount;
	}


	public void setFeeAmount(int feeAmount) {
		this.feeAmount = feeAmount;
	}


	@Override
	public String toString() {
		return "EntryFee [feeid=" + feeid + ", feeAmount=" + feeAmount + "]";
	}
	
	

}
