package com.virtusa.tms.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Point;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.util.DbConnection;

public class UserDAOImpl extends DbConnection {
	
	private static Logger log=LogManager.getLogger(UserDAOImpl.class);
	
	public List<Player> getAllPlayers() {
		ArrayList<Player> playerList = new ArrayList<>();
		String cmd1="select * from playerdetails";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd1);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int id=(rs.getInt(1));
				String playerName=(rs.getString(2));
				String playerDob=(rs.getString(3));
				int playerAge=(rs.getInt(4));
				String teamName=(rs.getString(5));
				String pbatStyle=(rs.getString(6));
				String pbowlStyle=(rs.getString(7));
				Player p=new Player(id,playerName,playerDob,playerAge,teamName,pbatStyle,pbowlStyle);
				playerList.add(p);
			}
			return playerList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return playerList;
	}
	public List<Team> getAllTeams(){
		String cmd="select * from team";
		ArrayList<Team> teamList=new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int teamid=rs.getInt(1);
				String teamName=rs.getString(2);
				String captain=rs.getString(3);
				String viceCaptain=rs.getString(4);
				Team t= new Team(teamid,teamName,captain,viceCaptain);
				teamList.add(t);
			}
			return teamList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return teamList;
	}
	public List<Point> getAllPoints(){
		String cmd="select * from points";
		ArrayList<Point> pointList=new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);){
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				String team=rs.getString(1);
				int played=rs.getInt(2);
				int win=rs.getInt(3);
				int lose=rs.getInt(4);
				int points=rs.getInt(5);
				Point p=new Point(team,played,win,lose,points);
				pointList.add(p);
			}
			return pointList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return pointList;
	}
	public List<Schedule>getAllSchedule(){
		String cmd="select * from schedule";
		ArrayList<Schedule> matchList=new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);){
			ResultSet rs =ps.executeQuery();
			while(rs.next()) {
				int matchid=rs.getInt(1);
				String teams=rs.getString(2);
				int overs=rs.getInt(3);
				String matchType=rs.getString(4);
				String time=rs.getString(5);
				String winner=rs.getString(6);
				String loser=rs.getString(7);
				Schedule m=new Schedule(matchid,teams,overs,matchType,time,winner,loser);
				matchList.add(m);			
		}
		return matchList;
	}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return matchList;
	}
	public List<EntryFee>getAllEntryFee(){
		String cmd="select * from entryfee";
		ArrayList<EntryFee> feeList= new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				int feeid=rs.getInt(1);
				int feeAmount=rs.getInt(2);
				EntryFee ef=new EntryFee(feeid,feeAmount);
				feeList.add(ef);
			}
			return feeList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return feeList;
	}
	public List<CskSquad> getAllCskSquad() {
		List<CskSquad> squadList1 = new ArrayList<>();
		String cmd="select * from csksquad";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int plaId=(rs.getInt(1));
				String plaName=(rs.getString(2));
				String plaDOB=(rs.getString(3));
				int plaAge=(rs.getInt(4));
				String plaBatStyle=(rs.getString(5));
				String plaBowlStyle=(rs.getString(6));
				CskSquad s=new CskSquad(plaId,plaName,plaDOB,plaAge,plaBatStyle,plaBowlStyle);
				squadList1.add(s);
			}
			return squadList1;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return squadList1;
	}
	public List<RcbSquad> getAllRcbSquad() {
		List<RcbSquad> squadList2 = new ArrayList<>();
		String cmd="select * from rcbsquad";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int playerid=(rs.getInt(1));
				String name=(rs.getString(2));
				String dOB=(rs.getString(3));
				int age=(rs.getInt(4));
				
				String batStyle=(rs.getString(5));
				String bowlStyle=(rs.getString(6));
				RcbSquad s=new RcbSquad(playerid,name,dOB,age,batStyle,bowlStyle);
				squadList2.add(s);
			}
			return squadList2;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return squadList2;
	}
	public List<DcSquad> getAllDcSquad() {
		List<DcSquad> squadList3 = new ArrayList<>();
		String cmd="select * from dcsquad";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int id=(rs.getInt(1));
				String dName=(rs.getString(2));
				String dcDOB=(rs.getString(3));
				int dAge=(rs.getInt(4));
				String dBatStyle=(rs.getString(5));
				String dBowlStyle=(rs.getString(6));
				DcSquad s=new DcSquad(id,dName,dcDOB,dAge,dBatStyle,dBowlStyle);
				squadList3.add(s);
			}
			return squadList3;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return squadList3;
	}
	public List<GtSquad> getAllGtSquad() {
		List<GtSquad> squadList4 = new ArrayList<>();
		String cmd="select * from gtsquad";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int gtId=(rs.getInt(1));
				String gtName=(rs.getString(2));
				String gtDOB=(rs.getString(3));
				int gtAge=(rs.getInt(4));
				String gtBatStyle=(rs.getString(5));
				String gtBowlStyle=(rs.getString(6));
				GtSquad s=new GtSquad(gtId,gtName,gtDOB,gtAge,gtBatStyle,gtBowlStyle);
				squadList4.add(s);
			}
			return squadList4;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return squadList4;
	}
	public List<SrhSquad> getAllSrhSquad() {
		List<SrhSquad> squadList = new ArrayList<>();
		String cmd="select * from srhsquad";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int plid=(rs.getInt(1));
				String pName=(rs.getString(2));
				String plDOB=(rs.getString(3));
				int plAge=(rs.getInt(4));
				String plBatStyle=(rs.getString(5));
				String plBowlStyle=(rs.getString(6));
				SrhSquad s=new SrhSquad(plid,pName,plDOB,plAge,plBatStyle,plBowlStyle);
				squadList.add(s);
			}
			return squadList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return squadList;
	}
	public Team searchTeamByName(String name) {
		String cmd="select * from team where teamname=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);) {
			ps.setString(1, name);
			ResultSet r=ps.executeQuery();
			if(r.next()) {
				int i=r.getInt(1);
				String team=r.getString(2);
				String cap =r.getString(3);
				String vc=r.getString(4);
				return new Team(i,team,cap,vc);
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return null;
	}
	public List<Schedule> searchScheduleByDate(String date) {
		List<Schedule> slist = new ArrayList<>();
		String cmd="select * from schedule where Matchtime=? ";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date parsed = format.parse(date);
			java.sql.Date sql = new java.sql.Date(parsed.getTime());
			ps.setDate(1, sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int id=rs.getInt(1);
				String teamName=rs.getString(2);
				int overs=rs.getInt(3);
				String matchType=rs.getString(4);
				String matchTime=rs.getString(5);
				String winner=rs.getString(6);
				String loser=rs.getString(7);
				Schedule  s=new Schedule(id,teamName,overs,matchType,matchTime,winner,loser);
				slist.add(s);
			}
			return slist;
		}
		catch(Exception i) {
			log.info(i.getMessage());
		}
		return slist;
	}
	
}
