package com.virtusa.tms.service;


import java.util.List;

import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Point;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;

public interface IUser {
	public List<Player>getAllPlayers();
	public List<Point>getAllPoints();
	public List<Schedule>getAllSchedule();
	public List<EntryFee>getAllEntry();
	public List<Team>getAllTeams();
	public List<CskSquad>getAllCskSquad();
	public List<RcbSquad>getAllRcbSquad();
	public List<DcSquad>getAllDcSquad();
	public List<GtSquad>getAllGtSquad();
	public List<SrhSquad>getAllSrhSquad();
	public Team searchTeamByName(String name);
	public List<Schedule> searchScheduleByDate(String date);

}
