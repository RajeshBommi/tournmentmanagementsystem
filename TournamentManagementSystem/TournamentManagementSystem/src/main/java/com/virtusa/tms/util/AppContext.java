package com.virtusa.tms.util;


import com.virtusa.tms.controller.AdminController;
import com.virtusa.tms.controller.AdminMenu;
import com.virtusa.tms.controller.CaptainController;
import com.virtusa.tms.controller.CaptainMenu;
import com.virtusa.tms.controller.LoginController;
import com.virtusa.tms.controller.MainMenu;
import com.virtusa.tms.controller.UserController;
import com.virtusa.tms.controller.UserMenu;
import com.virtusa.tms.dao.AdminDAOImpl;
import com.virtusa.tms.dao.CaptainDAOImpl;
import com.virtusa.tms.dao.UserDAOImpl;
import com.virtusa.tms.service.AdminServiceImpl;
import com.virtusa.tms.service.CaptainServiceImpl;
import com.virtusa.tms.service.UserServiceImpl;

public class AppContext {
	

private static AppContext appContext;

private AppContext() {
	super();
}
public static AppContext getInstance() {
	
	if(appContext==null) {
		appContext = new AppContext();
	}
	
	return appContext;
}
@SuppressWarnings("unchecked")
public <T> T getObject(String beanName) {
	if(beanName.equalsIgnoreCase("AdminService")) {
		return (T) new AdminServiceImpl();
	}
	else if(beanName.equalsIgnoreCase("CaptainService")) {
		return (T) new CaptainServiceImpl();
	}
	else if(beanName.equalsIgnoreCase("UserService")) {
		return (T) new UserServiceImpl();
	}
	else if(beanName.equalsIgnoreCase("IAdminDAOImpl")) {
		return (T) new AdminDAOImpl();
	}
	else if(beanName.equalsIgnoreCase("ICaptainDAOImpl")) {
		return (T) new CaptainDAOImpl();
	}
	else if(beanName.equalsIgnoreCase("IUserDAOImpl")) {
		return (T) new UserDAOImpl();
	}
	else if(beanName.equalsIgnoreCase("AdminController")) {
		return (T) new AdminController();
	}
	else if(beanName.equalsIgnoreCase("CaptainController")) {
		return (T) new CaptainController();
	}
	else if(beanName.equalsIgnoreCase("UserController")) {
		return (T) new UserController();
	}

	else if(beanName.equalsIgnoreCase("AdminMenu")) {
		return (T) new AdminMenu();
	}
	else if(beanName.equalsIgnoreCase("CaptainMenu")) {
		return (T) new CaptainMenu();
	}
	else if(beanName.equalsIgnoreCase("UserMenu")) {
		return (T) new UserMenu();
	}
	else if(beanName.equalsIgnoreCase("MainMenu")) {
		return (T) new MainMenu();
	}
	else if(beanName.equalsIgnoreCase("LoginController")) {
		return (T) new LoginController();
	}
	
	return null;
}
     
}

