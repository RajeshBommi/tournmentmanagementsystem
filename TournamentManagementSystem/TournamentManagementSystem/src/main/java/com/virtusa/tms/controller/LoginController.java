package com.virtusa.tms.controller;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.User;
import com.virtusa.tms.service.AdminServiceImpl;
import com.virtusa.tms.util.AppContext;



public class LoginController {
	
	Scanner sc = new Scanner(System.in);
	AdminServiceImpl  a= AppContext.getInstance().getObject("AdminService");
	AdminController ac= AppContext.getInstance().getObject("AdminController");
	UserController ul=AppContext.getInstance().getObject("UserController");
	CaptainController  cl= AppContext.getInstance().getObject("CaptainController");
	private static Logger log=LogManager.getLogger(LoginController.class);
	
	public void adminLogin() {
		log.info("Enter the AdminUserName");
        String uname=sc.next();
        log.info("Enter the Adminpassword");
        String upw=sc.next();
       int admin =a.adminLogin(uname, upw);
       if(admin==1) {
    	   User u=a.searchUserbyUsername(uname);
    		   if(u.getType().equals("Admin")&&u.getUserName().equals(uname)&&u.getUserPassword().equals(upw)) {
    			   log.info("Admin Logged in Successfully");
    		   ac.adminMenu();
    	         }
    		   else {
    			   log.info("The entered admin credentials are invalid");
    		   }
	}
       else {
    	   log.info("The credentials are invalid");
       }
	
	}
	
	public void captainLogin() {
		log.info("Enter the CaptainUserName");
        String uname=sc.next();
        log.info("Enter the Captainpassword");
        String upw=sc.next();
       int admin =a.adminLogin(uname, upw);
       if(admin==1) {
    	   User u=a.searchUserbyUsername(uname);
    	   if(u.getType().equalsIgnoreCase("Captain")&&u.getUserName().equals(uname)&&u.getUserPassword().equals(upw)) {
    		   log.info("Captain Logged in Successfully");
    		   cl.captainMenu();
    	         }
    	   else {
    		   log.info("The entered captain credentials are invalid");
    	   }
       }
       else {
      	   log.info("The entered  credentials are invalid");
         }
	}
	
	public void userLogin() {
		log.info("Enter the UserName");
        String uname=sc.next();
        log.info("Enter the password");
        String upw=sc.next();
       int admin =a.adminLogin(uname, upw);
       if(admin==1) {
    	   User u=a.searchUserbyUsername(uname);
    	   if(u.getType().equalsIgnoreCase("User")&&u.getUserName().equals(uname)&&u.getUserPassword().equals(upw)) {
    		   log.info("User Logged in Successfully");
    		   ul.userMenuList();
    	           }
    	   else {
    		   log.info("The entered user credentials are invalid");
    	   }
    	  
       }
       else {
      	   log.info("The above credentials are invalid");
         }
	}
	
	
}
