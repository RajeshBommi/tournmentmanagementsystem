package com.virtusa.tms.service;

import java.util.List;


import com.virtusa.tms.dao.UserDAOImpl;
import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Point;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.util.AppContext;

public class UserServiceImpl implements IUser {
	
	UserDAOImpl  u= AppContext.getInstance().getObject("IUserDAOImpl");

	@Override
	public List<Player> getAllPlayers() {
		return u.getAllPlayers();
	}

	@Override
	public List<Point> getAllPoints() {
	
		return u.getAllPoints();
	}

	@Override
	public List<Schedule> getAllSchedule() {
		
		return u.getAllSchedule();
	}

	@Override
	public List<EntryFee> getAllEntry() {
		return u.getAllEntryFee();
	}

	@Override
	public List<Team> getAllTeams() {
	 return u.getAllTeams();
	}

	
	public List<CskSquad> getAllCskSquad() {
		return u.getAllCskSquad();
	}

	@Override
	public List<RcbSquad> getAllRcbSquad() {
		return u.getAllRcbSquad();
	}

	@Override
	public List<DcSquad> getAllDcSquad() {
	return u.getAllDcSquad();
	}

	@Override
	public List<GtSquad> getAllGtSquad() {
	     return u.getAllGtSquad();
	}

	@Override
	public List<SrhSquad> getAllSrhSquad() {
		return u.getAllSrhSquad();
	}

	@Override
	public Team searchTeamByName(String name) {
		return u.searchTeamByName(name);
	}

	@Override
	public List<Schedule> searchScheduleByDate(String date) {
		return u.searchScheduleByDate(date);
	}
   
      
}
