package com.virtusa.tms.dto;

public class User {
	
	private String name;
    private	int usedid;
	private String userName;
	 private String userPassword;
	private String place;
	private String type;
	
	public User() {
		super();
	}

	public User(String name, int usedid, String userName, String userPassword, String place, String type) {
		super();
		this.name = name;
		this.usedid = usedid;
		this.userName = userName;
		this.userPassword = userPassword;
		this.place = place;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUsedid() {
		return usedid;
	}

	public void setUsedid(int usedid) {
		this.usedid = usedid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Admin [name=" + name + ", usedid=" + usedid + ", userName=" + userName + ", userPassword="
				+ userPassword + ", place=" + place + ", type=" + type + "]";
	}
}