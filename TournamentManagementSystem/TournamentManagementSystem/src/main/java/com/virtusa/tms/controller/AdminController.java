package com.virtusa.tms.controller;


import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.util.AppContext;




  public class AdminController {
	 
	 private static Logger log=LogManager.getLogger(AdminController.class);
		static Scanner sc=new Scanner(System.in);
	 

	public  void adminMenu() {
		int ch;
        AdminMenu am= AppContext.getInstance().getObject("AdminMenu");
        MainMenu mm= AppContext.getInstance().getObject("MainMenu");
        int flag=0;
		while(true) {
             am.amenu();
			ch=am.validateinpout();
			switch(ch) {
			case 1: am.searchTeam();
					break;
			case 2: am.modifyTeam();
					break;
			case 3: am.removeTeam();
					break;
			case 4:	am.getAllTeams();
					break;
			case 5:	am.addTeam();
					break;
			case 6:am.cancelMatch();
					break;
			case 7:am.addFee();
			       break;
			case 8:am.modifyFee();
			       break;
			case 9: am.removeFee();
			       break;
			case 10:am.getAllFee();
				 break;
			case 11:am.scheduleMatch();
			     break;
			case 12:am.getMatchDetails();
		         break;
			case 13:am.searchTeamName();
			  	break;
            case 14:mm.mainMenu();
                flag=1;
            	break;
			default: log.info("Invalid Option");
			}
			if(flag==1) {
				break;
			}
		}
		sc.close();
	}
  }
