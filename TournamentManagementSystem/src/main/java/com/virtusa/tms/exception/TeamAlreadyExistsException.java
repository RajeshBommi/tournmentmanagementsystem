package com.virtusa.tms.exception;

public class TeamAlreadyExistsException extends Exception{
	public TeamAlreadyExistsException(String s) {
		super (s);
	}

}
