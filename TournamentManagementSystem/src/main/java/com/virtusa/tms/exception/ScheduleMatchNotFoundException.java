package com.virtusa.tms.exception;

public class ScheduleMatchNotFoundException extends Exception {
	public ScheduleMatchNotFoundException(String s) {
		super(s);
	}

}
