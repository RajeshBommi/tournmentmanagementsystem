package com.virtusa.tms.exception;

public class TeamNotFoundException extends Exception{
	
	public TeamNotFoundException(String s) {
		super(s);
	}

}
