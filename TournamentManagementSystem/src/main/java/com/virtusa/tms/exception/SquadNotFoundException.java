package com.virtusa.tms.exception;

public class SquadNotFoundException extends Exception {
	
	public SquadNotFoundException(String s) {
		super(s);
	}

}
