package com.virtusa.tms.exception;

public class PlayerAlreadyExistsException extends Exception{
	public PlayerAlreadyExistsException (String s) {
		super(s);
	}

}
