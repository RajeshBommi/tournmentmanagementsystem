package com.virtusa.tms.controller;


import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.service.UserServiceImpl;
import com.virtusa.tms.util.AppContext;

public class UserController extends UserServiceImpl {
	
	private static Logger log=LogManager.getLogger(UserController.class);
	 int ch=0;
	Scanner sc=new Scanner(System.in);
	
	 UserMenu um=AppContext.getInstance().getObject("UserMenu");
	 MainMenu mm = AppContext.getInstance().getObject("MainMenu");
	 
	public  void userMenuList() {
		do {
			um.umenu();
			ch=um.validateinput();
			
			switch(ch) {
			case 1:um.getAllPlayers();
			       break;
			       
			case 2:um.getAllPoints();
			      break;
			case 3:um.getAllFixtures();
			     break;
			case 4:um.getAllFee();
			       break;
			case 5:um.getAllTeams();
			       break;
			case 6:um.getAllRcb();
			       break;
			case 7:um.getAllCsk();
		       break;
			case 8:um.getAllDc();
		       break;
			case 9:um.getAllGt();
		       break;
			case 10:um.getAllSrh();
		       break;
			case 11: um.getTeamByName();
			break;
			case 12: um.getSearchScheduleByDate();
			break;
			case 13:return;
			default:log.info("Invalid Option");
			}
			
		}while(ch<13);
		sc.close();
	}

}
