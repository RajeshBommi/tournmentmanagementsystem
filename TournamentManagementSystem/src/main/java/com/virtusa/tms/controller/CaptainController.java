package com.virtusa.tms.controller;





import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.util.AppContext;


public class CaptainController  {
	
	static Scanner sc= new Scanner(System.in);
	private static Logger log=LogManager.getLogger(CaptainController.class);
	

	CaptainMenu cm=AppContext.getInstance().getObject("CaptainMenu");
	
	public void captainMenu() { 
		cm.cmenu();
		int ch=0;
	do {
		ch=cm.inpout();
		
		switch(ch) {
		case 0:log.info("");
		break;
	case 1:cm.addPlayer();
		     break;
	case 2:cm.searchPlayer();
	       break;
	case 3:cm.modifyPlayer();
			break;
	case 4:cm.removePlayer();
	       break;
	case 5:cm.getAllPlayers();
	      break;
	case 6:cm.getAllTeams();
	      break;
	case 7:return;
	default: log.info("Invalid Option");
	}	
	}
	while(ch<7);
	sc.close();

}
}
