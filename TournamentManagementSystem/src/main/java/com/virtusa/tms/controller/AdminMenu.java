package com.virtusa.tms.controller;



import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.exception.FeeNotFoundException;
import com.virtusa.tms.exception.ScheduleMatchNotFoundException;
import com.virtusa.tms.exception.TeamAlreadyExistsException;
import com.virtusa.tms.exception.TeamNotFoundException;
import com.virtusa.tms.service.AdminServiceImpl;
import com.virtusa.tms.util.AppContext;


public class AdminMenu {
	
	int teamid;
	int matchid;
	String teams;
	String teamName;
	String captain;
	String viceCaptain;
	int feeid;
	int feeAmount;
	int overs;
	String matchType;
	String time;
	String winner;
	String loser;
		
	AdminServiceImpl  a= AppContext.getInstance().getObject("AdminService");

	
	private static Logger log=LogManager.getLogger(AdminMenu.class);
	Scanner sc=new Scanner(System.in);
	
	
	
	 public int validateinpout()
	  {
		  String choice = sc.next();
		  int ch ;
		  if(choice.matches("[0-9]*"))
			  {
			   	ch = Integer.parseInt(choice);
			  	return ch;
			  }
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return validateinpout();
		  }
	  }
	 public String validatestringinpout()
	  {
		  String choice = sc.next();
		  
		  if(choice.matches("[a-zA-Z]*"))
			  	return choice;
			  
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return validatestringinpout();
		  }
	  }
	 public void amenu() {
			log.info("** Admin Management Menu **");
		    log.info("1.Search Team");
			log.info("2.Modify Team");
			log.info("3.Remove Team");
			log.info("4.List all Teams");
			log.info("5.Add Team");
			log.info("6.Cancel Match");
			log.info("7.Add fee");
			log.info("8.Modify fee");
			log.info("9.Remove fee");
			log.info("10.Get fee");
			log.info("11.Schedule Match");
			log.info("12.Search Match by id");
			log.info("13.Get Team by id");
			log.info("14.Back to main menu");
			log.info("Enter your Option");
	 }
	 
	public void searchTeam() {
		log.info("Enter the Teamid to be searched: ");
		teamid=validateinpout();
		Team s=a.searchTeam(teamid);
		if(s==null) {
			try {
				throw new TeamNotFoundException("Team does not exist");
			}
			catch(Exception e) {
				log.error(e);
				}
		}
		else {
			log.info(s);
		}
	}
	public void modifyTeam() {
		log.info("Enter the Teamid to be modified: ");
		teamid=validateinpout();
		if(a.searchTeam(teamid)!=null) {
			log.info("Enter new TeamName");
			teamName=validatestringinpout();
			log.info("Enter new Captain");
			captain=validatestringinpout();
			log.info("Enter new ViceCaptain");
			viceCaptain=validatestringinpout();
			Team t=new Team(teamid,teamName,captain,viceCaptain); 
			if(a.modifyTeam(t)) {
				log.info("Team details modified successfully");
			}
			else {
				log.error("Team details modified Failed");
			}
		}
		else {
			try {
			throw new TeamNotFoundException("Sorry Teamid does not exist");
		}
			catch(Exception e) {
				log.error(e);
			}
			}
	}
	public void removeTeam() {
		log.info("Enter the Team id to be removed: ");
		teamid=validateinpout();
		log.info("Enter team name");
		String tname=validatestringinpout();
		if(a.removeTeam(teamid,tname)) {
			log.info("Team details removed successfully");
		}
		else {
			try {
				throw new TeamNotFoundException("Sorry Teamid does not exist in table");
			}
			catch(Exception t) {
				log.error(t);
				
			}
		}
	}

	
	public void getAllTeams() {
		List<Team> tl=a.getAllTeams();
        if(!tl.isEmpty()) {
        	tl.stream().forEach(log::info);
        }
        else {
        	try {
        		throw new TeamNotFoundException("Teams doesn't exist");
        	}
        	catch(Exception o) {
        		log.error(o);
        	}
        }
	}
	public void addTeam() {
		log.info("Enter the Teamid: ");
		teamid=validateinpout();
		if(a.searchTeam(teamid) == null) {
		log.info("Enter Teamname");
		teamName=validatestringinpout();
		log.info("Enter Captain Name");
		captain=validatestringinpout();
		log.info("Enter ViceCaptain Name");
		viceCaptain=validatestringinpout();
		Team t1=new Team(teamid,teamName,captain,viceCaptain);
		if(a.addTeam(t1)) {
			log.info("Team added successfully");
		}
		else {
			log.error("Sorry team does not exist");
		}
      }
		else {
			try {
				throw new TeamAlreadyExistsException("Team alreadyexists");
				}
			catch(Exception r) {
				log.error(r);
			}
		}
			
	}
	public void cancelMatch() {
		log.info("Enter Matchid to Cancel the match");
		matchid=validateinpout();
		if(a.cancelMatch(matchid)) {
			log.info("Match Cancelled due to rain");
			}
		else {
			try {
				throw new ScheduleMatchNotFoundException("Sorry Matchid does not exist");
			}
			catch(Exception a1) {
				log.error(a1);
			}
		}
	}
	public void addFee() {
		if(a.getFee()==null) {
			   log.info("Enter feeid");
		       feeid=validateinpout();
		       log.info("Enter the fee Amount");
		       feeAmount=validateinpout();
		       EntryFee ef=new EntryFee(feeid,feeAmount);
		       if(a.addFee(ef)) {
		    	   log.info("Fee details added Successfully");
		       }
		       else {
		    	   log.info("Fee details doesn't exist");
		       }
		       }
		       else {
		    	   try {
		    		   throw new FeeNotFoundException("Fee details doesn't exist");
		    	   }
		    	   catch(Exception w) {
		    		   log.error(w);
		    	   }
		       }
	}
	public void modifyFee() {
		log.info("Enter the feeid to be modified");
	       feeid=validateinpout();
	       EntryFee e=(EntryFee) a.getFee();
	       if(e!=null) {
	    	   log.info("Enter the Fee Amount");
	    	   feeAmount=validateinpout();
	    	   e=new EntryFee(feeid,feeAmount);
	    	   if(a.modifyFee(e)) {
	    		   log.info("Fee Details modifed successfully");
	    		    }
	    	   else {
	    		   log.info("Fee details can't be modified");
	    	   }
	       }
	       else {
	    		try {
					throw new FeeNotFoundException("Sorry Feeid does not exist");
				}
					catch(Exception c) {
						log.error(c);
					}
	       }
	}
	public void removeFee() {
		log.info("Enter the feeid to be removed");
	       feeid=validateinpout();
	       if(a.removeFee(feeid)) {
	    	   log.info("Fee Amount removed successfully");
	       }
	       else {
	    	   try {
	    	   throw new FeeNotFoundException("Feeid doesn't exist");
	    	   }
	    	   catch(Exception i) {
	    		   log.error(i);
	    	   }
	       }
	}
	public void getAllFee() {
		List<EntryFee> el=a.getFee();
        if(el==null) {
        	try {
        		throw new FeeNotFoundException("Fee not exists");
        	}
        	catch(Exception n) {
        		log.error(n);
        	}
        }
        else {
        	log.info(el);
        }
	}
	public void scheduleMatch() {
		log.info("Enter the Matchid to schedule the match");
        matchid=validateinpout();
        if(a.getMatchById(matchid) == null) {
        sc.nextLine();	
        log.info("Enter team names");
        teams=sc.nextLine();
        log.info("Enter no.of Overs");
        overs=validateinpout();
        log.info("Enter the type of match");
        matchType=sc.next();
        sc.nextLine();
        log.info("Enter Match timings ");
         time=sc.next();
        log.info("Enter the winner team Name");
        sc.nextLine();
        winner=sc.next();
        log.info("Enter the loser team name");
        loser=sc.next();
        if(a.scheduleMatch( new Schedule(matchid,teams,overs,matchType,time,winner,loser))) {
        	log.info("The match is scheduled between two teams");
        }
        else {
        	log.info("Can't schedule the match");
        }
        }
        else {
        	try {
        		throw new ScheduleMatchNotFoundException("The match is not Scheduled");
        	}
        	catch(Exception s1) {
        		log.error(s1);
        	}
        }
	}
	public void getMatchDetails() {
		log.info("Enter the Matchid to get the details");
        matchid=validateinpout();
        if(a.getMatchById(matchid) == null) {
       	 try {
					throw new ScheduleMatchNotFoundException("Match does not exist");
				}
				catch(Exception x) {
					log.error(x);
					}
       	 }
        else {
       	 log.info("Match details fetched successfully");
       	 log.info(a.getMatchById(matchid));
        }
	}
	public void searchTeamName() {
		log.info("Enter the Teamid to get the details");
        teamid=validateinpout();
        if(a.searchTeam(teamid) == null) {
       	 try {
					throw new TeamNotFoundException("Team does not exist");
				}
				catch(Exception x) {
					log.error(x);
					}
       	 }
        else {
       	 log.info("Team details fetched successfully");
       	 log.info(a.searchTeam(teamid));
        }
	}


}