package com.virtusa.tms.controller;

import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import com.virtusa.tms.dto.EntryFee;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Point;
import com.virtusa.tms.dto.Schedule;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;

import com.virtusa.tms.exception.FeeNotFoundException;
import com.virtusa.tms.exception.PlayerNotFoundException;
import com.virtusa.tms.exception.ScheduleMatchNotFoundException;
import com.virtusa.tms.exception.TeamNotFoundException;

import com.virtusa.tms.service.UserServiceImpl;
import com.virtusa.tms.util.AppContext;

public class UserMenu {
	
	String teamName;
	String date;
	
	private static Logger log=LogManager.getLogger(UserMenu.class);
	Scanner sc=new Scanner(System.in);
	
   UserServiceImpl  u= AppContext.getInstance().getObject("UserService");
	 
	 public int validateinput()
	  {
		  String choice = sc.next();
		  int ch ;
		  if(choice.matches("[0-9]*"))
			  {
			   	ch = Integer.parseInt(choice);
			  	return ch;
			  }
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return validateinput();
		  }
	  }
	 public String validatestringinput()
	  {
		  String choice = sc.next();
		  
		  if(choice.matches("[a-zA-Z]*"))
			  	return choice;
			  
		  else {
			  log.info("you have entered incorrect details... please enter valid details");
			  return validatestringinput();
		  }
	  }
	 public void umenu() {
		 log.info("********User Management**********");
			log.info("1.List of Players");
			log.info("2.Points Table");
			log.info("3.Fixtures");
			log.info("4.EntryFee Table");
			log.info("5.List Of Teams");
			log.info("6.List of Squad1-RCB");
			log.info("7.List of Squad2-CSK");
			log.info("8.List of Squad3-DC");
			log.info("9.List of Squad4-GT");
			log.info("10.List of Squad5-SRH");
			log.info("11.Team By Name");
			log.info("12.Search Schedule By Date");
			log.info("13.Back to MainMenu");
		    log.info("Enter Your Option");
	 }
	 public void getAllPlayers() {
		 List<Player> pl=u.getAllPlayers();
	       if(!pl.isEmpty()) {
	    	   pl.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new PlayerNotFoundException("Players doesn't exist");
	    	   }
	    	   catch(Exception e) {
	    		   log.info(e.getMessage());
	    	   }
	       }
	 }
	 public void getAllPoints() {
		 List<Point> p=u.getAllPoints();
	       if(!p.isEmpty()) {
	    	   p.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new PlayerNotFoundException("Points doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	       }
	 }
	 public void getAllFixtures() {
		 List<Schedule> m=u.getAllSchedule();
	     if(!m.isEmpty()) {
	    	 m.stream().forEach(i->log.info(i));
	     }
	     else {
	    	   try {
	    		   throw new ScheduleMatchNotFoundException("Schedule doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	     }
	 }
	 public void getAllFee() {
		 List<EntryFee> ef=u.getAllEntry();
	       if(!ef.isEmpty()) {
	    	   ef.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new FeeNotFoundException("Fee details doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	       }
	 }
	 public void getAllTeams() {
		 List<Team> t=u.getAllTeams();
	       if(!t.isEmpty()) {
	    	   t.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new TeamNotFoundException("Team details doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	       }
	 }
	 public void getAllRcb() {
		 List<RcbSquad> s1=u.getAllRcbSquad();
	       if(!s1.isEmpty()) {
	    	   s1.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new TeamNotFoundException("RCB Team details doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	       }
	 }
	 public void getAllCsk() {
		 List<CskSquad> s2=u.getAllCskSquad();
	       if(!s2.isEmpty()) {
	    	   s2.stream().forEach(i->log.info(i));
	       }
     else {
  	   try {
  		   throw new TeamNotFoundException("CSK Team details doesn't exist");
  	   }
  	   catch(Exception x) {
  		 log.info(x.getMessage());
  	   }
     }
	 }
	 public void getAllDc() {
		 List<DcSquad> s3=u.getAllDcSquad();
	       if(!s3.isEmpty()) {
	    	   s3.stream().forEach(i->log.info(i));
	       }
     else {
  	   try {
  		   throw new TeamNotFoundException("DC Team details doesn't exist");
  	   }
  	   catch(Exception x) {
  		 log.info(x.getMessage());
  	   }
     }
	 }
	 public void getAllGt() {
		 List<GtSquad> s4=u.getAllGtSquad();
	       if(!s4.isEmpty()) {
	    	   s4.stream().forEach(i->log.info(i));
	       }
	       else {
	    	   try {
	    		   throw new TeamNotFoundException("GT Team details doesn't exist");
	    	   }
	    	   catch(Exception x) {
	    		   log.info(x.getMessage());
	    	   }
	       }
	 }
	 public void getAllSrh() {
		 List<SrhSquad> s5=u.getAllSrhSquad();
	        if(!s5.isEmpty()) {
	        	s5.stream().forEach(i->log.info(i));
	        }
	        else {
		    	   try {
		    		   throw new TeamNotFoundException("SRH Team details doesn't exist");
		    	   }
		    	   catch(Exception x) {
		    		   log.info(x.getMessage());
		    	   }
		       }
	 }
	 public void getTeamByName() {
		 log.info("Enter the Teamname to be searched: ");
			teamName=validatestringinput();
			Team s=u.searchTeamByName(teamName);
			if(s==null) {
				try {
					throw new TeamNotFoundException("Team does not exist");
				}
				catch(Exception e) {
					log.error(e);
					}
			}
			else {
				log.info(s);
			}
	 }
	 public void getSearchScheduleByDate() {
		 log.info("Enter the date to be searched: ");
			date=sc.next();
			List<Schedule> sl=u.searchScheduleByDate(date);
			if(sl==null) {
				try {
					throw new ScheduleMatchNotFoundException("Schedule does not exist");
				}
				catch(Exception e) {
					log.error(e);
					}
			}
			else {
				log.info(sl);
			}
	 }
}
