package com.virtusa.tms.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.virtusa.tms.dto.User;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.CskSquad;
import com.virtusa.tms.dto.RcbSquad;
import com.virtusa.tms.dto.DcSquad;
import com.virtusa.tms.dto.GtSquad;
import com.virtusa.tms.dto.SrhSquad;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.util.DbConnection;

public class CaptainDAOImpl extends DbConnection{
	
	private static Logger log=LogManager.getLogger(CaptainDAOImpl.class);
	
	public boolean addPlayer(Player p) {
		String cmd="insert into playerdetails values(?,?,?,?,?,?,?)";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(1,p.getPlayerid());
			ps.setString(2,p.getName());
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			Date parsed = format.parse(p.getdOB());
			java.sql.Date sql = new java.sql.Date(parsed.getTime());
			ps.setDate(3, sql);
			ps.setInt(4, p.getAge());
			ps.setString(5,p.getTeam());
			ps.setString(6,p.getBatStyle());
			ps.setString(7,p.getBowlStyle());
			ps.executeUpdate();
			return true;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public Player searchPlayer(int playerid) {
		String cmd="select * from playerdetails where playerid=?";
		try(PreparedStatement ps =getConnection().prepareStatement(cmd);)
		{
			Player p=null;
			ps.setInt(1, playerid);
			ResultSet rs=ps.executeQuery();
			while (rs.next()) {
				int playerid1=rs.getInt(1);
				String name=rs.getString(2);
				java.sql.Date dob=rs.getDate(3);
				SimpleDateFormat sdf= new SimpleDateFormat("yyyy/MM/dd");
				String dOB=sdf.format(dob);
				int age=rs.getInt(4);
				String team=rs.getString(5);
				String batStyle=rs.getString(6);
				String bowlStyle=rs.getString(7);
				p=new Player(playerid1,name,dOB,age,team,batStyle,bowlStyle);
				return p;
			}
		}
		catch(Exception e) {
			log.info(e.getMessage()); 
		}
		return null;
	}
	public boolean removePlayer(int playerid) {
		String cmd="delete from playerdetails where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(1,playerid);
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifyPlayer(Player p) {
		String cmd="update playerdetails set playerName=?,DateofBirth=?,Age=?,Team=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(7,p.getPlayerid());
			ps.setString(1,p.getName());
			ps.setString(2, p.getdOB());
			ps.setInt(3,p.getAge());
			ps.setString(4, p.getTeam());
			ps.setString(5,p.getBatStyle());
			ps.setString(6,p.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public List<Player> getAllPlayers() {
		List<Player> plList = new ArrayList<>();
		String cmd="select * from playerdetails";
		
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int playerid=(rs.getInt(1));
				String name=(rs.getString(2));
				String dOB=(rs.getString(3));
				int age=(rs.getInt(4));
				String team=(rs.getString(5));
				String batStyle=(rs.getString(6));
				String bowlStyle=(rs.getString(7));
				Player p=new Player(playerid,name,dOB,age,team,batStyle,bowlStyle);
				plList.add(p);
			}
			return plList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return plList;
	}
	public List<Team> getAllTeams(){
		String cmd="select * from team";
		ArrayList<Team> tList=new ArrayList<>();
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				int tid=rs.getInt(1);
				String tName=rs.getString(2);
				String cName=rs.getString(3);
				String vcName=rs.getString(4);
				Team t= new Team(tid,tName,cName,vcName);
				tList.add(t);
			}
			return tList;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return tList;
	}
	public boolean modifySquad1(CskSquad s1) {
		String cmd="update csksquad set playerName=?,DateofBirth=?,Age=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(6,s1.getPlayerid());
			ps.setString(1,s1.getName());
			ps.setString(2, s1.getdOB());
			ps.setInt(3,s1.getAge());
			ps.setString(4,s1.getBatStyle());
			ps.setString(5,s1.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifySquad2(RcbSquad s2) {
		String cmd="update srhsquad set playerName=?,DateofBirth=?,Age=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(6,s2.getPlayerid());
			ps.setString(1,s2.getName());
			ps.setString(2, s2.getdOB());
			ps.setInt(3,s2.getAge());
			ps.setString(4,s2.getBatStyle());
			ps.setString(5,s2.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifySquad3(DcSquad s3) {
		String cmd="update dcsquad set playerName=?,DateofBirth=?,Age=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(6,s3.getPlayerid());
			ps.setString(1,s3.getName());
			ps.setString(2, s3.getdOB());
			ps.setInt(3,s3.getAge());
			ps.setString(4,s3.getBatStyle());
			ps.setString(5,s3.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifySquad4(GtSquad s4) {
		String cmd="update gtsquad set playerName=?,DateofBirth=?,Age=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(6,s4.getPlayerid());
			ps.setString(1,s4.getName());
			ps.setString(2, s4.getdOB());
			ps.setInt(3,s4.getAge());
			ps.setString(4,s4.getBatStyle());
			ps.setString(5,s4.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean modifySquad5(SrhSquad s5) {
		String cmd="update rcbsquad set playerName=?,DateofBirth=?,Age=?,BatStyle=?,BowlStyle=? where playerid=?";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setInt(6,s5.getPlayerid());
			ps.setString(1,s5.getName());
			ps.setString(2, s5.getdOB());
			ps.setInt(3,s5.getAge());
			ps.setString(4,s5.getBatStyle());
			ps.setString(5,s5.getBowlStyle());
			int n=ps.executeUpdate();
			if(n>0) {
				return true;
			}
			
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		return false;
	}
	public boolean register(User a) {
		String cmd="insert into admin values(?,?,?,?,?,?)";
		try(PreparedStatement ps=getConnection().prepareStatement(cmd);)
		{
			ps.setString(1,a.getName());
			ps.setInt(2, a.getUsedid());
			ps.setString(3, a.getUserName());
			ps.setString(4, a.getUserPassword());
			ps.setString(5, a.getPlace());
			ps.setString(6, a.getType());
			ps.executeUpdate();
			return true;
		}
		catch(Exception e) {
			log.info(e.getMessage());
		}
		
	return false;


	}
}
