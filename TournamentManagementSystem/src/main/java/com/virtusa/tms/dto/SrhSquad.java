package com.virtusa.tms.dto;

public class SrhSquad {
	
	private int srhId;
	private String srhName;
	private String srhDOB;
	private int srhAge;
	private String sBatStyle;
	private String sBowlStyle;
	
	public SrhSquad() {
		super();
	}

	public SrhSquad(int playerid, String name, String dOB, int age, String batStyle, String bowlStyle) {
		super();
		this.srhId = playerid;
		this.srhName = name;
		this.srhDOB = dOB;
		this.srhAge = age;
		this.sBatStyle = batStyle;
		this.sBowlStyle = bowlStyle;
	}

	public int getPlayerid() {
		return srhId;
	}

	public void setPlayerid(int playerid) {
		this.srhId = playerid;
	}

	public String getName() {
		return srhName;
	}

	public void setName(String name) {
		this.srhName = name;
	}

	public String getdOB() {
		return srhDOB;
	}

	public void setdOB(String dOB) {
		this.srhDOB = dOB;
	}

	public int getAge() {
		return srhAge;
	}

	public void setAge(int age) {
		this.srhAge = age;
	}

	public String getBatStyle() {
		return sBatStyle;
	}

	public void setBatStyle(String batStyle) {
		this.sBatStyle = batStyle;
	}

	public String getBowlStyle() {
		return sBowlStyle;
	}

	public void setBowlStyle(String bowlStyle) {
		this.sBowlStyle = bowlStyle;
	}

	@Override
	public String toString() {
		return "Player [playerid=" + srhId + ", name=" + srhName + ", dOB=" + srhDOB + ", age=" + srhAge 
				+ ", batStyle=" + sBatStyle + ", bowlStyle=" + sBowlStyle + "]";
	}

}
