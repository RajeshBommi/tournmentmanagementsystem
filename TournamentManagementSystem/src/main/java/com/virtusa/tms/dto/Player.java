package com.virtusa.tms.dto;



public class Player {
	
	private int playerid;
	private String name;
	private String dOB;
	private int age;
	private String team;
	private String batStyle;
	private String bowlStyle;
	
	public Player() {
		super();
	}

	public Player(int playerid, String name, String dOB, int age, String team, String batStyle, String bowlStyle) {
		super();
		this.playerid = playerid;
		this.name = name;
		this.dOB = dOB;
		this.age = age;
		this.team = team;
		this.batStyle = batStyle;
		this.bowlStyle = bowlStyle;
	}

	public int getPlayerid() {
		return playerid;
	}

	public void setPlayerid(int playerid) {
		this.playerid = playerid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getdOB() {
		return dOB;
	}

	public void setdOB(String dOB) {
		this.dOB = dOB;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getBatStyle() {
		return batStyle;
	}

	public void setBatStyle(String batStyle) {
		this.batStyle = batStyle;
	}

	public String getBowlStyle() {
		return bowlStyle;
	}

	public void setBowlStyle(String bowlStyle) {
		this.bowlStyle = bowlStyle;
	}

	@Override
	public String toString() {
		return "Player [playerid=" + playerid + ", name=" + name + ", dOB=" + dOB + ", age=" + age + ", team=" + team
				+ ", batStyle=" + batStyle + ", bowlStyle=" + bowlStyle + "]";
	}
	
	
	
	
	
	
}