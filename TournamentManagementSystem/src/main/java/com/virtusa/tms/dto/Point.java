package com.virtusa.tms.dto;

public class Point {
	private String team;
	private int played;
	private int win;
	private int lose;
	private int points;
	
	
	public Point() {
		super();
	}


	public Point(String team, int played, int win, int lose, int points) {
		super();
		this.team = team;
		this.played = played;
		this.win = win;
		this.lose = lose;
		this.points = points;
	}


	public String getTeam() {
		return team;
	}


	public void setTeam(String team) {
		this.team = team;
	}


	public int getPlayed() {
		return played;
	}


	public void setPlayed(int played) {
		this.played = played;
	}


	public int getWin() {
		return win;
	}


	public void setWin(int win) {
		this.win = win;
	}


	public int getLose() {
		return lose;
	}


	public void setLose(int lose) {
		this.lose = lose;
	}


	public int getPoints() {
		return points;
	}


	public void setPoints(int points) {
		this.points = points;
	}


	@Override
	public String toString() {
		return "Point [team=" + team + ", played=" + played + ", win=" + win + ", lose=" + lose + ", points=" + points
				+ "]";
	}
	
}