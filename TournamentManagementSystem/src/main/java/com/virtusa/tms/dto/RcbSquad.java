package com.virtusa.tms.dto;

public class RcbSquad {
	
	private int rcbId;
	private String rcbName;
	private String rcbDOB;
	private int rcbAge;
	private String rBatStyle;
	private String rBowlStyle;
	
	public RcbSquad() {
		super();
	}

	public RcbSquad(int playerid, String name, String dOB, int age,  String batStyle, String bowlStyle) {
		super();
		this.rcbId = playerid;
		this.rcbName = name;
		this.rcbDOB = dOB;
		this.rcbAge = age;
		this.rBatStyle = batStyle;
		this.rBowlStyle = bowlStyle;
	}

	public int getPlayerid() {
		return rcbId;
	}

	public void setPlayerid(int playerid) {
		this.rcbId = playerid;
	}

	public String getName() {
		return rcbName;
	}

	public void setName(String name) {
		this.rcbName = name;
	}

	public String getdOB() {
		return rcbDOB;
	}

	public void setdOB(String dOB) {
		this.rcbDOB = dOB;
	}

	public int getAge() {
		return rcbAge;
	}

	public void setAge(int age) {
		this.rcbAge = age;
	}


	public void setBatStyle(String batStyle) {
		this.rBatStyle = batStyle;
	}
	public String getBatStyle() {
		return rBatStyle;
	}

	public String getBowlStyle() {
		return rBowlStyle;
	}

	public void setBowlStyle(String bowlStyle) {
		this.rBowlStyle = bowlStyle;
	}

	@Override
	public String toString() {
		return "Player [playerid=" + rcbId + ", name=" + rcbName + ", dOB=" + rcbDOB + ", age=" + rcbAge + ", batStyle=" + rBatStyle + ", bowlStyle=" + rBowlStyle + "]";
	}



}
