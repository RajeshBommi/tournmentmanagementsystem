package com.virtusa.tms.dto;

public class Schedule {
	private int matchid;
	private String teams;
	private int overs;
	private String matchType;
	private String time;
	private String winner;
	private String loser;
	public Schedule() {
		super();
	}
	public Schedule(int matchid, String teams, int overs, String matchType, String time, String winner, String loser) {
		super();
		this.matchid = matchid;
		this.teams = teams;
		this.overs = overs;
		this.matchType = matchType;
		this.time = time;
		this.winner = winner;
		this.loser = loser;
	}
	public int getMatchid() {
		return matchid;
	}
	public void setMatchid(int matchid) {
		this.matchid = matchid;
	}
	public String getTeams() {
		return teams;
	}
	public void setTeams(String teams) {
		this.teams = teams;
	}
	public int getOvers() {
		return overs;
	}
	public void setOvers(int overs) {
		this.overs = overs;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getWinner() {
		return winner;
	}
	public void setWinner(String winner) {
		this.winner = winner;
	}
	public String getLoser() {
		return loser;
	}
	public void setLoser(String loser) {
		this.loser = loser;
	}
	@Override
	public String toString() {
		return "Schedule [matchid=" + matchid + ", teams=" + teams + ", overs=" + overs + ", matchType=" + matchType
				+ ", time=" + time + ", winner=" + winner + ", loser=" + loser + "]";
	}
	
	
	
	
}
