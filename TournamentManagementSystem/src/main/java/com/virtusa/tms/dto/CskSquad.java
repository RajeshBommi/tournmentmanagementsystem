package com.virtusa.tms.dto;

public class CskSquad {
	
	private int pid;
	private String pName;
	private String pDOB;
	private int pAge;
	private String pBatStyle;
	private String pBowlStyle;
	
	public CskSquad() {
		super();
	}
	public CskSquad(int playerid, String name, String dOB, int age,  String batStyle, String bowlStyle) {
		super();
		this.pid = playerid;
		this.pName = name;
		this.pDOB = dOB;
		this.pAge = age;
		this.pBatStyle = batStyle;
		this.pBowlStyle = bowlStyle;
	}
	public int getPlayerid() {
		return pid;
	}
	public void setPlayerid(int playerid) {
		this.pid = playerid;
	}
	public String getName() {
		return pName;
	}
	public void setName(String name) {
		this.pName = name;
	}
	public String getdOB() {
		return pDOB;
	}
	public void setdOB(String dOB) {
		this.pDOB = dOB;
	}
	public int getAge() {
		return pAge;
	}
	public void setAge(int age) {
		this.pAge = age;
	}
	
	public String getBatStyle() {
		return pBatStyle;
	}
	public void setBatStyle(String batStyle) {
		this.pBatStyle = batStyle;
	}
	public String getBowlStyle() {
		return pBowlStyle;
	}
	public void setBowlStyle(String bowlStyle) {
		this.pBowlStyle = bowlStyle;
	}
	@Override
	public String toString() {
		return "Squad1 [playerid=" + pid + ", name=" + pName + ", dOB=" + pDOB + ", age=" + pAge +", batStyle=" + pBatStyle + ","
				+ " bowlStyle=" + pBowlStyle + "]";
	}
	
	

}
