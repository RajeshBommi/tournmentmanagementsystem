package com.virtusa.tms.dto;

public class GtSquad {
	
	private int gtId;
	private String gtName;
	private String gtDOB;
	private int gtAge;
	private String gtBatStyle;
	private String gtBowlStyle;
	
	public GtSquad() {
		super();
	}

	public GtSquad(int playerid, String name, String dOB, int age,  String batStyle, String bowlStyle) {
		super();
		this.gtId = playerid;
		this.gtName = name;
		this.gtDOB = dOB;
		this.gtAge = age;
		this.gtBatStyle = batStyle;
		this.gtBowlStyle = bowlStyle;
	}

	public int getPlayerid() {
		return gtId;
	}

	public void setPlayerid(int playerid) {
		this.gtId = playerid;
	}

	public String getName() {
		return gtName;
	}

	public void setName(String name) {
		this.gtName = name;
	}

	public String getdOB() {
		return gtDOB;
	}

	public void setdOB(String dOB) {
		this.gtDOB = dOB;
	}

	public int getAge() {
		return gtAge;
	}

	public void setAge(int age) {
		this.gtAge = age;
	}

	public String getBatStyle() {
		return gtBatStyle;
	}

	public void setBatStyle(String batStyle) {
		this.gtBatStyle = batStyle;
	}

	public String getBowlStyle() {
		return gtBowlStyle;
	}

	public void setBowlStyle(String bowlStyle) {
		this.gtBowlStyle = bowlStyle;
	}

	@Override
	public String toString() {
		return "Player [playerid=" + gtId + ", name=" + gtName + ", dOB=" + gtDOB + ", age=" + gtAge 
				+ ", batStyle=" + gtBatStyle + ", bowlStyle=" + gtBowlStyle + "]";
	}

}
