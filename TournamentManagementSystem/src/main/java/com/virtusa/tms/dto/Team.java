package com.virtusa.tms.dto;

public class Team {
	
	private int teamid;
	private String teamName;
	private String captain;
	private String viceCaptain;
	
	
	public Team() {
		super();
	}


	public Team(int teamid, String teamName, String captain, String viceCaptain) {
		super();
		this.teamid = teamid;
		this.teamName = teamName;
		this.captain = captain;
		this.viceCaptain = viceCaptain;
	}


	public int getTeamid() {
		return teamid;
	}


	public void setTeamid(int teamid) {
		this.teamid = teamid;
	}


	public String getTeamName() {
		return teamName;
	}


	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}


	public String getCaptain() {
		return captain;
	}


	public void setCaptain(String captain) {
		this.captain = captain;
	}


	public String getViceCaptain() {
		return viceCaptain;
	}


	public void setViceCaptain(String viceCaptain) {
		this.viceCaptain = viceCaptain;
	}


	@Override
	public String toString() {
		return "Team [teamid=" + teamid + ", teamName=" + teamName + ", captain=" + captain + ", viceCaptain="
				+ viceCaptain + "]";
	}
	

}
