package com.virtusa.tms.service;


import java.util.List;


import com.virtusa.tms.dao.CaptainDAOImpl;
import com.virtusa.tms.dto.Player;
import com.virtusa.tms.dto.Team;
import com.virtusa.tms.util.AppContext;

public  class CaptainServiceImpl implements ICaptain {
	
	CaptainDAOImpl  cl= AppContext.getInstance().getObject("ICaptainDAOImpl");
	
	public boolean addPlayer(Player p) {
	return cl.addPlayer(p);
	}
	public Player searchPlayer(int playerid) {
		return cl.searchPlayer(playerid);
	}
	public boolean modifyPlayer(Player p) {
		return cl.modifyPlayer(p);
	}
	public boolean removePlayer(int playerid) {
		return cl.removePlayer(playerid);
		}
	public List<Player> getAllPlayers(){
		return cl.getAllPlayers();
	}
	
	public List<Team> getAllTeams() {
		return cl.getAllTeams();
	}




}
