package com.virtusa.tms.service;

import java.util.List;


import com.virtusa.tms.dto.Player;

import com.virtusa.tms.dto.Team;

public interface ICaptain  {
	public boolean addPlayer(Player p);
	public Player searchPlayer(int playerid);
	public boolean modifyPlayer(Player p);
	public boolean removePlayer(int playerid);
	public List<Player> getAllPlayers();
	public List<Team> getAllTeams();
	
}
